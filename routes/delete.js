const router = require('express').Router()
const Product = require('../models/product')


router.get('/', (req, res) => {
    res.sendStatus(404)
})


router.get('/:productName', (req, res) => {
    const productName = req.params.productName
    Product.findOneAndDelete({
        productName: productName
    }, (err) => {

        if(err)
            console.log(err)
        else
            res.redirect('/')
    })
})

module.exports = router