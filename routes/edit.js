const express = require('express');
const router = express.Router();
const Product = require('../models/product')

/* GET home page. */
router.get('/:productName', function(req, res, next) {

    const productName = req.params.productName.trim();
    console.log(productName)
    const query = Product.where({
        productName: productName
    })
    query.findOne((err,data) => {

        if(err)
            res.sendStatus(500)
        else{

            res.render('edit', { title: 'Express', result: data });
        }
    })


});

//Route parameter for getting the specific value to be edited
router.post('/:productName', (req, res) => {

    //Object destructuring
    const {productname, price, description} = req.body;
    if(isNaN(price))
        res.render('edit', {show: true, errormessage: 'Please enter correct price'})
    else {

        Product.findOneAndUpdate({
            productName: req.params.productName
        }, {

            productName: productname,
            price: price,
            description: description
        }, (err)=> {

            if(err)
                console.log(err)
            else
                res.redirect('/')
        })

    }
})

module.exports = router;