var express = require('express');
var router = express.Router();
var Product = require('../models/product')

/* GET home page. */
router.get('/', function(req, res, next) {

  //to fetch all the data to the UI using Product Schema object
  Product.find({},(err,data) => {

    if(err)
      res.sendStatus(500)
    else{
      res.render('index', { title: 'Express', products: data });
    }
  })
});

module.exports = router;
