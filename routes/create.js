let router = require('express').Router()
const Product = require('../models/product')

router.get('/', (req, res) => {

    res.render('create')
})

router.post('/', (req, res) => {

    const product = {

        productName: req.body.productname.trim(),
        price: req.body.price,
        description: req.body.description
    }
    if(product.productName === '' || product.price === '' || product.description === '') {
        res.render('create', {errormessage: 'Please enter all input fields', show: true})
    }else {

        const newProduct = new Product({
            productName: product.productName,
            price: product.price,
            description: product.description
        })

        newProduct.save(newProduct, (err)=> {
            if(err)
                console.log(err)
            res.redirect('/');
        })
    }
})

module.exports = router